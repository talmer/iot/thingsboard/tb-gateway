#!/bin/bash

# Copying env variables into conf files
printenv | awk -F "=" '{print "export " $1 "='\''" $2 "'\''"}' >> /usr/share/tb-gateway/conf/tb-gateway.conf

echo ">tb-gateway.conf"
cat /usr/share/tb-gateway/conf/tb-gateway.conf

echo ">tb-gateway.yml"
cat /usr/share/tb-gateway/conf/tb-gateway.yml

echo "ls /usr/share/tb-gateway/conf/"
ls -a /usr/share/tb-gateway/conf/

echo "Starting 'TB-gateway' service..."
exec java -jar /usr/share/tb-gateway/bin/tb-gateway.jar