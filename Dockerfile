FROM thingsboard/openjdk8

ENV DEBIAN_FRONTEND=noninteractive
ARG TB_GATEWAY_VERSION=2.2.1rc

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install curl -y \
    && apt-get clean

RUN curl -L https://github.com/thingsboard/thingsboard-gateway/releases/download/${TB_GATEWAY_VERSION}/tb-gateway-${TB_GATEWAY_VERSION}.deb -o /tb-gateway.deb \
    && dpkg -i /tb-gateway.deb \
    && rm /tb-gateway.deb

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
CMD  /entrypoint.sh
